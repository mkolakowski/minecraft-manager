#!/bin/bash
# Things to be added
# Server mover, how to move server to different folder
# Server renamer, copies foldes to new directory
#--------------------------------------------------------------------------------------------------------------------
        scriptFunctionArgs=$2
        projectURL="https://gitlab.com/mkolakowski/minecraft-manager"
        projectFile="https://gitlab.com/mkolakowski/minecraft-manager/raw/master/mcmanager.sh"
# ConfigDirectory Stores the script config file location
        ConfigDirectory="/root/.mcconfig"
# ConfigLocation Stores the script config file location
        configLocation="$ConfigDirectory/$1.config"
        upgradescript=false
# Script version
        ScriptVersion="2020-03-14.2205"
# Minecraft Server version
        MinecraftServerVersion=1.5.2
# Minecraft Server Jar URL
        ServerJARURL="https://launcher.mojang.com/v1/objects/bb2b6b1aefcd70dfd1892149ac3a215f6c636b07/server.jar"
# Start time of the script
        starttime=$(date +%Y%m%d-%H%M%S)
# $MCServerName Servers name
        MCServerName=$1
# MCLocation - Location of Minecraft Directory
        MCLocation="/media/minecraft/$MCServerName"
# MCBackupLocal - Local Location Minecraft Backup to be placed
        MCBackupLocal="/media/minecraft/backup/$MCServerName"        
# MCBackupRemote - Rclone Location Minecraft Backup to be placed, Leave Blank if not using
        MCBackupRemote=
# MCScreenSession - Name of the minecraft session to be created
        MCScreenSession=$MCServerName
# MCmemorySmall - Minimum memory used in GB
        MCmemorySmall="1G"
# MCmemoryMax - Minimum memory used in GB
        MCmemoryMax="2G"
#--------------------------------------------------------------------------------------------------------------------
TZ='America/New_York'; export TZ
#--------------------------------------------------------------------------------------------------------------------
# Outputs Script Version
function functionStartMinecraftServer () {
    screen -d -m -S $MCScreenSession java -Xms$MCmemorySmall -Xmx$MCmemoryMax -jar $MCLocation/server.jar nogui
} #End
#--------------------------------------------------------------------------------------------------------------------
# Outputs Script Version
function functionShowScriptVersion () {
    echo " "
    echo "      |-------------------------------------|"
    echo "      | Current version:    $ScriptVersion |"
    echo "      |-------------------------------------|"    
    echo " "
} #End
#--------------------------------------------------------------------------------------------------------------------
# Creates Configuration with defaults from variables
function functionCreateConfig () {
    echo "Creating config at: $configLocation"
    echo "------------------"
    echo "
    #Project URL:  $projectURL
    #Go to the URL to download the latest version
            confScriptVersion=$ScriptVersion
    # $MCServerName Servers name
            confMCServerName=$MCServerName
    # MCLocation - Location of Minecraft Directoty
            confMCLocation=$MCLocation
    # MCBackupLocal - Local Location Minecraft Backup to be placed
            confMCBackupLocal=$MCBackupLocal
    # MCBackupRemote - Rclone Location Minecraft Backup to be placed, Leave Blank if not using
            confMCBackupRemote=$MCBackupRemote
    # MCScreenSession - Name of the minecraft session to be created
            confMCScreenSession=$MCScreenSession
    # MCmemorySmall - Minimum memory used in GB
            confMCmemorySmall=$MCmemorySmall
    # MCmemoryMax - Minimum memory used in GB
            confMCmemoryMax=$MCmemoryMax
    " >> $configLocation
} #End
#--------------------------------------------------------------------------------------------------------------------
# Prints Configuration
function functionPrintConfig() {
    functionShowScriptVersion
    echo "      MCServerName        $MCServerName"
    echo "      MCLocation          $MCLocation"
    echo "      MCBackupLocal       $MCBackupLocal"
    echo "      MCBackupRemote      $MCBackupRemote"
    echo "      configLocation      $configLocation"
    echo "      MCScreenSession     $MCScreenSession"
    echo "      MCmemorySmall       $MCmemorySmall"
    echo "      MCmemoryMax         $MCmemoryMax"
} #End
#--------------------------------------------------------------------------------------------------------------------
function functionBackupConfig () {

    sudo mkdir -p $MCLocation/bakupConfig
    cp $configLocation $MCLocation/bakupConfig/configbackup-$starttime.OLD
    echo "Backup Config located at: $MCLocation/bakupConfig/configbackup-$starttime.OLD"
    echo "---------------------------------------------------------------------------"
} #End
#--------------------------------------------------------------------------------------------------------------------
# Copies values from configuration into script
function functionLoadConfigVariables () {
    #tells the program to refrence this file
    . $configLocation
    MCServerName=$confMCServerName
    MCLocation=$confMCLocation
    MCBackupLocal=$confMCBackupLocal
    MCBackupRemote=$confMCBackupRemote
    MCScreenSession=$confMCScreenSession
    MCmemorySmall=$confMCmemorySmall
    MCmemoryMax=$confMCmemoryMax
} #End
#--------------------------------------------------------------------------------------------------------------------
function functionUpdateScript() {
    echo "-------------------------------"
    if [[ $upgradescript = true ]];
        then
            if [[ ! -f ./minecraft.sh ]]; 
                then
                    echo "No Script to remove"
                    echo "-------------------"
                else
                    echo "Removing old script"
                    echo "-------------------"
                    sudo rm ./minecraft.sh
                fi
        fi
    if [[ ! -f ./minecraft.sh ]]; 
    then
        wget -q $projectFile
        if [[ $? -ne 0 ]];
            then
                echo "There was an error downloading the latest release"
                echo "-------------------------------------------------"
            else
                echo "Latest release Downloaded, Rebuilding Config"
                echo "--------------------------------------------"
                
                #functionBackupConfig
                #functionLoadConfigVariables
                #rm $configLocation
                #functionCreateConfig
            fi
        sudo chmod u+x ./minecraft.sh
        fi
} #End
#--------------------------------------------------------------------------------------------------------------------
function functionChangeMinecraftName () {
    read -p "Do you want to change the server name from $MCServerName? (y/n)" boolminecraftservernameedit
        echo "------------------------------------------------------"
        if [[ $boolminecraftservernameedit == "y" ]] || [[ $boolminecraftservernameedit == "Y" ]];
            then
                read -p "What do you want it to be?" MCServerName
                echo "------------------------------------------------------"
                echo "Server name changed to $MCServerName"
                echo "------------------------------------------------------"
            fi
    } #End
#--------------------------------------------------------------------------------------------------------------------
function functionupdateosapps () {
    #tested 2019-10-21 - mkolakowski - works as intended
    echo "Performing updates"
    echo "------------------"
    apt-get update && apt-get upgrade -f --assume-yes
    } #End
#--------------------------------------------------------------------------------------------------------------------
# Reads config and asks user if they want to modify it
function functionReadConfig() {
    functionLoadConfigVariables
    functionPrintConfig
    echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    echo "!! Please verify that the config is correct before proceding  !!"
    echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    read -p "Do you want to edit the config before proceding? (y/n)" boolconfigedit
    echo "------------------------------------------------------"
    if [[ $boolconfigedit == "y" ]] || [[ $boolconfigedit == "Y" ]];
        then
            echo "Launching text editor"
            echo "---------------------"
            nano $configLocation
            functionLoadConfigVariables
            functionPrintConfig
        fi
} #End
#--------------------------------------------------------------------------------------------------------------------
# stores config data and re-creates config with user data
function functionReloadConfig (){
    read -p "Are you sure you want to rebuild the config? (y/n)" boolconfigreload
    echo    "------------------------------------------------------"
    if [[ $boolconfigreload == "y" ]] || [[ $boolconfigreload == "Y" ]];
        then
            functionLoadConfigVariables #Loading the latest variables
            functionPrintConfig
            functionBackupConfig
            rm $configLocation
            functionCreateConfig
            functionPrintConfig
        else 
            echo "No changes made to configuration file"
            echo "-------------------------------------"
        fi
} #End
#--------------------------------------------------------------------------------------------------------------------
function functioninstallminecrafterver () {
    #checking if JAVA is installed
    javainstallstatus=$(dpkg-query -W --showformat='${Status}\n' openjdk-8-jre-headless:amd64 2>/dev/null)
    if [[ "x${javainstallstatus}" != "xinstall ok installed" ]]; 
        then 
            if apt-get -qq install -y apt-openjdk-8-jre-headless:amd64 >/dev/null; 
                then
                    sudo apt update
                    echo "java installed"
                    dpkg-query -W --showformat='${Version}\n' openjdk-8-jre-headless:amd64
                    echo "-----------------------------"
                    fi
            fi
    #checking if Minecraft Server is installed
        if [[ ! -f  $MCLocation/server.jar ]]; 
            then
                echo "Downloading Minecraft Jar"
                echo "-------------------------------------------"
                cd $MCLocation
                wget $ServerJARURL
                chmod u+x server.jar
            else
                echo "Updating Minecraft Server Jar"
                echo "-------------------------------------------"
                cd $MCLocation
                wget $ServerJARURL
                chmod u+x server.jar
            fi
} #End
#--------------------------------------------------------------------------------------------------------------------
function functioninstallfirewall () {
    #checking if Fail2Ban is installed
    fail2baninstallstatus=$(dpkg-query -W --showformat='${Status}\n' fail2ban 2>/dev/null)
    if [[ "x${fail2baninstallstatus}" != "xinstall ok installed" ]]; 
        then 
            if apt-get -qq install -y fail2ban >/dev/null; 
                then
                    echo "Fail2Ban installed"
                    echo "------------------"
                    fi
            fi
    #checking if UFW Firewall is installed
    ufwinstallstatus=$(dpkg-query -W --showformat='${Status}\n' ufw 2>/dev/null)
    if [[ "x${ufwinstallstatus}" != "xinstall ok installed" ]]; 
        then 
            if apt-get -qq install -y ufw >/dev/null; 
                then
                    sudo ufw status verbose
                    sudo ufw allow ssh
                    sudo ufw allow 25565/tcp
                    # Turning on Firewall
                    sudo ufw enable
                    echo "Firewall installed"
                    echo "------------------"
                    fi    
            fi
} #End
#--------------------------------------------------------------------------------------------------------------------
# Reads config and asks user if they want to modify it
function functionRcloneConfig() {
    functionLoadConfigVariables
    functionPrintConfig
    read -p "Do you want to edit the config before proceding? (y/n)" boolconfigrcloneedit
    echo "------------------------------------------------------"
    if [[ $boolconfigrcloneedit == "y" ]] || [[ $boolconfigrcloneedit == "Y" ]];
        then
            rclone config
            fi
} #End
#--------------------------------------------------------------------------------------------------------------------
function functioninstallrclone () {
    # Installing rclone if you want to backup remotly
    curl https://rclone.org/install.sh | sudo bash
    # Edit Rclone
    functionRcloneConfig
    echo "Adding the Rclone Mount Service"
    echo "-------------------------------"
    echo "Starting Rclone Service"
    echo "-----------------------"
    systemctl start $rcloneservice1
    echo "Enabling Rclone Service"
    echo "-----------------------"
    systemctl enable $rcloneservice1
}
#--------------------------------------------------------------------------------------------------------------------
function functionMinecraftPropertyEditor () {
    functionLoadConfigVariables
    nano $MCLocation/server.properties
} #End

#--------------------------------------------------------------------------------------------------------------------
function functionUpdateMemory () {
    read -p "Do you want to the MINIMUM memory allocation from $MCmemorySmall? (y/n)" boolconfigrcloneedit
    echo "------------------------------------------------------"
    if [[ $boolconfigrcloneedit == "y" ]] || [[ $boolconfigrcloneedit == "Y" ]];
        then
            read -p "What is the new MINIMUM memory allocation? (example 1G)" MCmemorySmall
            echo "The new minimum memory allocation is $MCmemorySmall"
            echo " "
            fi
    read -p "Do you want to the MAXIMUM memory allocation from $MCmemoryMax? (y/n)" boolconfigrcloneedit
    echo "------------------------------------------------------"
    if [[ $boolconfigrcloneedit == "y" ]] || [[ $boolconfigrcloneedit == "Y" ]];
        then
            read -p "What is the new MAXIMUM memory allocation? (example 1G and must be larger than $MCmemorySmall)" MCmemoryMax
            echo "The new maximum memory allocation is $MCmemoryMax"
            echo " "
            fi
} #End
#--------------------------------------------------------------------------------------------------------------------
function functionminecraftbackup () {
    functionLoadConfigVariables
    #Creatng Variable that stores zip naming stratagy
    MCBackupZip=$MCBackupLocal/backup-$MCServerName--$starttime.zip
    echo "---------------------------------------------------------------------------"
    echo "Starting Backup"
            mkdir -p $MCBackupLocal
            mkdir -p $MCLocation/bakupConfig
            mkdir -p $MCLocation/backupCrontab
            mkdir -p $MCLocation/crash-reports
            mkdir -p $MCLocation/logs
            mkdir -p $MCLocation/logs-latest
            mkdir -p $MCLocation/logs-rclone 
    echo "---------------------------------------------------------------------------"
    echo "Turn off Auto-Save + Save Game"
            screen -r $MCScreenSession -X stuff 'say Starting Backup\n'
            screen -r $MCScreenSession -X stuff 'say save-off\n'
            screen -r $MCScreenSession -X stuff 'say save-all\n'
    echo "---------------------------------------------------------------------------"
    echo "Copying Live Server Log"
            cp $MCLocation/logs/latest.log $MCLocation/logs-latest/$MCServerName-latest-$starttime.log
    echo "---------------------------------------------------------------------------"
    echo "Copying Server config"
            cp $configLocation $MCLocation/bakupConfig/config-$MCServerName-$starttime.config
    echo "---------------------------------------------------------------------------"
    echo "Copying Server config"
            crontab -l > $MCLocation/backupCrontab/crontab-$MCServerName-$starttime.config
    echo "---------------------------------------------------------------------------"
    echo "Zip Minecraft Folder"
            #zip -r $MCBackupLocal $MCLocation -x $MCBackupZip --exclude="*/.*"
            zip -r $MCBackupZip $MCLocation --exclude="*/.*"
    echo "---------------------------------------------------------------------------"
    echo "Turn on Auto-Save"
            screen -r $MCScreenSession -X stuff 'say Backup Complete\n'
            screen -r $MCScreenSession -X stuff 'say save-on\n'
    echo "---------------------------------------------------------------------------"
    echo "Turn on Auto-Save"
            screen -r $MCScreenSession -X stuff 'say Backup Complete\n'
            screen -r $MCScreenSession -X stuff 'say save-on\n'
    echo "---------------------------------------------------------------------------"
    echo "Upload to Cloud via Rclone"
            rclone copy -P $MCBackupLocal $MCBackupRemote/$MCServerName/Backups/$(date +%Y-%m-%d) --log-file=$MCLocation/logs-rclone/$MCServerName-rclone-$starttime.log
    echo "---------------------------------------------------------------------------"
    echo "Remove Backup Zips older than 14 days"
            find $MCBackupLocal -type f -name "*.zip" -mtime +14 -exec rm {} \;
    echo "---------------------------------------------------------------------------"
    echo "Backup Server Logs to Cloud"
            rclone copy -P $MCLocation/logs $MCBackupRemote/$MCServerName/Logs-Server
            rclone copy -P $MCLocation/logs-latest $MCBackupRemote/$MCServerName/Logs-Latest
            rclone copy -P $MCLocation/crash-reports  $MCBackupRemote/$MCServerName/Crash-Reports
    echo "---------------------------------------------------------------------------"
    echo "Removing logs older than 14 Days"
            find $MCLocation/logs -type f -name "*.gz" -mtime +14 -exec rm {} \;
            find $MCLocation/logs-latest -type f -name "*.gz" -mtime +14 -exec rm {} \;
            find $MCLocation/crash-reports -type f -name "*.gz" -mtime +14 -exec rm {} \;
    echo "---------------------------------------------------------------------------"
    ls -l $MCBackupLocal
    echo " "
    echo "Job Started:  $starttime"
    echo "Job Ended:    $(date +%Y%m%d-%H%M%S)"
    echo " "
} #End
#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------
# Downloads Fresh script if none present
if [[ ! -f ./minecraft.sh ]]; 
    then
        echo "Downloading latest release"
        echo "--------------------------"
        wget -q $projectFile
        sudo chmod u+x ./minecraft.sh

        fi
#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------
# Config Loader/Generator    
#    if [[ ! -f  $configLocation ]]; 
#    then
#        if [[ ! -f $MCLocation/config ]];
#            then
#                sudo mkdir -p $MCLocation/config
#                echo "Creating Config Directory"
#                echo "-------------------------"
#            else
#                # making the congig
#                functionCreateConfig    
#        fi
#    fi
#--------------------------------------------------------------------------------------------------------------------
if [[ ! -f $configLocation ]]; 
    then
        echo $configLocation
        sudo mkdir -p $ConfigDirectory
        echo "Listing current configs"
        echo "-----------------------"
        ls -l $ConfigDirectory
        echo "------------------------------------------------------"
        read -p "Do you want to genrerate $configLocation? (y/n)" boolminecraftconfiggeneration
        echo "------------------------------------------------------"
            if [[ $boolminecraftconfiggeneration == "y" ]] || [[ $boolminecraftconfiggeneration == "Y" ]];
                then
                    functionCreateConfig
                fi
        fi
#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------
#functionLoadConfigVariables
#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------
# Choice Logic
if [[ "$scriptFunctionArgs" == "install" ]];
    then
        echo "Installing Minecraft Server"
        echo "----------------------"
        functionReadConfig
        functionChangeMinecraftName
        functionLoadConfigVariables
        functionupdateosapps
        functioninstallminecrafterver
        functioninstallfirewall
        functioninstallrclone
        functionStartMinecraftServer
        nano $MCLocation/eula.txt
    #--------------------------------------------------------------------------------------------------------------------
    elif [[ "$scriptFunctionArgs" == "start" ]];
        then
            echo "Starting Minecraft Server"
            echo "--------------------"
            cd $MCLocation
            functionStartMinecraftServer
            sed -n '30p' < $MCLocation/server.properties
            sed -n '29p' < $MCLocation/server.properties
    #--------------------------------------------------------------------------------------------------------------------
    elif [[ "$2scriptFunctionArgs" == "restart" ]];
        then
            echo "Restarting Minecraft Server"
            echo "----------------------"
            screen -r $MCScreenSession -X stuff 'say Stoping server now\n'
            screen -r $MCScreenSession -X stuff 'say stop\n'
            cd $MCLocation
            functionStartMinecraftServer
    #--------------------------------------------------------------------------------------------------------------------
    elif [[ "$scriptFunctionArgs" == "update" ]];
        then
            functionupdateosapps
            functioninstallminecrafterver
    #--------------------------------------------------------------------------------------------------------------------
    elif [[ "$2" == "updateself" ]];
        then
            upgradescript=true
            functionUpdateScript 
    #--------------------------------------------------------------------------------------------------------------------
    elif [[ "$scriptFunctionArgs" == "stop" ]];
        then
            screen -r $MCScreenSession -X stuff 'say Stoping server now\n'
            screen -r $MCScreenSession -X stuff '\n say stop\n'
            screen -r $MCScreenSession
    #--------------------------------------------------------------------------------------------------------------------
    elif [[ "$scriptFunctionArgs" == "config" ]];
        then
            functionReadConfig
    #--------------------------------------------------------------------------------------------------------------------
    elif [[ "$scriptFunctionArgs" == "backup" ]];
        then
            echo "Backing up the Minecraft Server"
            functionminecraftbackup
    #--------------------------------------------------------------------------------------------------------------------
    elif [[ "$scriptFunctionArgs" == "reload" ]];
        then
            functionReloadConfig
    #--------------------------------------------------------------------------------------------------------------------
    elif [[ "$scriptFunctionArgs" == "connect" ]];
        then
                screen -r $MCScreenSession
    #--------------------------------------------------------------------------------------------------------------------
    elif [[ "$scriptFunctionArgs" == "mcprop" ]];
        then
            functionMinecraftPropertyEditor
   #--------------------------------------------------------------------------------------------------------------------
    elif [[ "$scriptFunctionArgs" == "memory" ]];
        then
            functionUpdateMemory
            functionBackupConfig
            rm $configLocation
            functionCreateConfig
            functionPrintConfig
   #--------------------------------------------------------------------------------------------------------------------
    elif [[ "$scriptFunctionArgs" == "version" ]];
        then
            functionShowScriptVersion   
#--------------------------------------------------------------------------------------------------------------------
    elif [[ "$scriptFunctionArgs" == "EULA" ]];
        then
            nano $MCLocation/eula.txt
    #--------------------------------------------------------------------------------------------------------------------
    else
        functionLoadConfigVariables
        functionPrintConfig
        echo " "
        echo "No command found. Here is a list of commands:"
        echo " |------------|----------------------------------------------------------------------------------------|"
        echo " | backup     | Performs a backup of the script config and Minecraft Server if desired                 |"
        echo " | config     | Prints Script Config on screen and gives option to edit in nano                        |"
        echo " | connect    | Conects to Minecraft Server running in screensession                                   |"
        echo " | EULA       | Opens EULA file to accept the Minecraft server EULA                                    |"
        echo " | install    | Performs the install of the Minecraft Server, Fail2Ban and UFW Firewall                |"
        echo " | mcprop     | Opens text editor to edit the Minecraft Server Properties                              |"
        echo " | memory     | Allows you to change the minimum and maximum memory for the minecraft server           |"
        echo " | reload     | Renames current config and creates a new config with users variables                   |"
        echo " | restart    | Performs a Restart on the Minecraft Server                                             |"
        echo " | restore    | Copies Files from the backup location to restore the Script Config                     |"
        echo " | start      | Performs a manual start of the Minecraft Server                                        |"
        echo " | stop       | Performs a hard stop of the Minecraft Server                                           |"
        echo " | update     | Updates OS and apps                                                                    |"
        echo " | updateself | Deletes and downloads the latest version of this script then rebuilds config           |"
        echo " | version    | Displays the version of the script                                                     |"
        echo " |------------|----------------------------------------------------------------------------------------|"
        echo " "
    fi
#--------------------------------------------------------------------------------------------------------------------
#2020-03-14.2205 - First Stable Release

